/*
 * This file is a part of the El project: <https://gitlab.com/Grzesiek11/El>
 * El is available under the terms of the GNU Affero General Public License, version 3 or later.
 * The license is distributed with the project, but if you don't have a copy, see <https://www.gnu.org/licenses/agpl-3.0.html> instead.
 */

use std::collections::HashMap;

use serde::Deserialize;

#[derive(Deserialize)]
pub struct Config {
    #[serde(rename = "discordToken")]
    pub discord_token: String,
    #[serde(rename = "mongodbUri")]
    pub mongodb_uri: String,
    pub database: String,
    pub plugins: HashMap<String, Value>,
}

#[derive(Deserialize)]
#[serde(untagged)]
pub enum Value {
    Map(HashMap<String, Value>),
    String(String),
    U64(u64),
}

impl Value {
    pub fn try_as_map(&self) -> Result<&HashMap<String, Value>, ()> {
        match self {
            Self::Map(value) => Ok(value),
            _ => Err(()),
        }
    }

    pub fn try_as_string(&self) -> Result<&String, ()> {
        match self {
            Self::String(value) => Ok(value),
            _ => Err(()),
        }
    }

    pub fn try_as_u64(&self) -> Result<&u64, ()> {
        match self {
            Self::U64(value) => Ok(value),
            _ => Err(()),
        }
    }
}
