/*
 * This file is a part of the El project: <https://gitlab.com/Grzesiek11/El>
 * El is available under the terms of the GNU Affero General Public License, version 3 or later.
 * The license is distributed with the project, but if you don't have a copy, see <https://www.gnu.org/licenses/agpl-3.0.html> instead.
 */

pub fn sanitize_markdown<'a>(s: &String) -> String {
    let mut result = String::new();

    let mut escaped = false;
    for ch in s.chars() {
        if ['*', '_', '~', '>', '`', '|'].contains(&ch) && !escaped {
            result.push('\\');
        }

        result.push(ch);

        if ch == '\\' && !escaped {
            escaped = true;
        } else {
            escaped = false;
        }
    }

    result
}
