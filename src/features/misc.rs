/*
 * This file is a part of the El project: <https://gitlab.com/Grzesiek11/El>
 * El is available under the terms of the GNU Affero General Public License, version 3 or later.
 * The license is distributed with the project, but if you don't have a copy, see <https://www.gnu.org/licenses/agpl-3.0.html> instead.
 */

use std::collections::HashMap;

use serenity::{model::id::ChannelId, prelude::Mentionable};

use crate::events::{Context, Command, EventResult, Plugin, CommandMetadata};

async fn ping(ctx: Context, cmd: Command) -> EventResult {
    cmd.msg.channel_id.say(&ctx.c.http, "Pong!").await?;

    Ok(())
}

async fn move_discussion(ctx: Context, cmd: Command) -> EventResult {
    let source_channel_id = cmd.msg.channel_id;
    let target_channel_id = ChannelId(cmd.arguments["channel"].value()?.as_u64().unwrap());

    let mut target_msg = target_channel_id.say(&ctx.c.http, "...").await?;

    let source_msg = source_channel_id.send_message(&ctx.c.http, |m| m.add_embed(
        |e| e.description(format!("➡️ {}\n[Jump to message]({})", target_channel_id.mention(), target_msg.link()))
    )).await?;

    target_msg.edit(&ctx.c.http, |m| {
        m.content("")
            .add_embed(
                |e| e.description(format!("**Moved from** {}\n[Jump to message]({})", source_channel_id.mention(), source_msg.link()))
            )
    }).await?;

    Ok(())
}

pub fn register() -> Plugin {
    Plugin {
        events: Vec::new(),
        commands: HashMap::from([
            ("ping".to_string(), cmdparse::Node::Command(cmdparse::Command {
                arguments: HashMap::new(),
                subcommands: HashMap::new(),
                parse_negative_numbers: false,
                metadata: Box::new(CommandMetadata {
                    callback: Some(Box::new(move |context, command| Box::pin(ping(context, command)))),
                }),
            })),
            ("move".to_string(), cmdparse::Node::Command(cmdparse::Command {
                arguments: HashMap::from([
                    ("channel".to_string(), cmdparse::Argument {
                        kind: cmdparse::ArgumentKind::Positional(0),
                        value: cmdparse::Value::U64,
                        default: None,
                        repeating: cmdparse::Repeat::False,
                        required: true,
                    }),
                ]),
                subcommands: HashMap::new(),
                parse_negative_numbers: false,
                metadata: Box::new(CommandMetadata {
                    callback: Some(Box::new(move |context, command| Box::pin(move_discussion(context, command)))),
                }),
            })),
        ]),
    }
}
