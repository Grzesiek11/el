/*
 * This file is a part of the El project: <https://gitlab.com/Grzesiek11/El>
 * El is available under the terms of the GNU Affero General Public License, version 3 or later.
 * The license is distributed with the project, but if you don't have a copy, see <https://www.gnu.org/licenses/agpl-3.0.html> instead.
 */

use std::collections::HashMap;

use regex::Regex;
use serenity::{prelude::Mentionable, model::prelude::OnlineStatus};
use chrono::{DateTime, Utc, NaiveDateTime, SecondsFormat};

use crate::events::{Context, Command, EventResult, Plugin, CommandMetadata};

fn format_timestamp(timestamp: DateTime<Utc>) -> String {
    format!("<t:{}:f> (`{}`) ||`{}`||", timestamp.timestamp(), timestamp.to_rfc3339_opts(SecondsFormat::Millis, true), timestamp.timestamp_millis())
}

fn format_snowflake(snowflake: u64, header: bool) -> String {
    let discord_milis = snowflake >> 22;
    let unix_milis = discord_milis + 1420070400000;
    let timestamp = DateTime::from_utc(NaiveDateTime::from_timestamp(
        (unix_milis / 1000) as i64,
        (unix_milis % 1000 * 1000000) as u32
    ), Utc);
    let worker_id = snowflake << 64 - 22 >> 64 - 5;
    let process_id = snowflake << 64 - 17 >> 64 - 5;
    let increment = snowflake << 64 - 12 >> 64 - 12;

    let mut result = String::new();
    if header {
        result.push_str("\n\n__Snowflake info:__\n");
    }
    result.push_str(&format!(concat!(
        "**Timestamp:** {}\n",
        "**Discord timestamp:** `{}`\n",
        "**Worker ID:** {}\n",
        "**Process ID:** {}\n",
        "**Increment:** {}",
    ), format_timestamp(timestamp), discord_milis, worker_id, process_id, increment));

    result
}

fn list_flags(flags: Vec<&str>) -> String {
    let mut result = String::new();

    if !flags.is_empty() {
        let mut flags_iter = flags.iter().peekable();
        while let Some(flag) = flags_iter.next() {
            result.push_str(&format!("**{}**", flag));
            if flags_iter.peek().is_some() {
                result.push_str(", ")
            }
        }
        result.push_str("\n");
    }

    result
}

async fn user(ctx: Context, cmd: Command) -> EventResult {
    let user = match cmd.arguments["user"].try_value() {
        Some(user_id) => ctx.c.http.get_user(user_id.as_u64()?).await?,
        _ => ctx.c.http.get_user(*cmd.msg.author.id.as_u64()).await?,
    };

    let mut title = format!("👤 User info: `{}`", user.tag());
    if user.bot {
        title.push_str(" 🇧​🇴​🇹");
    }

    let mut body = String::new();

    if let Some(guild) = cmd.msg.guild(&ctx.c.cache) {
        if let Ok(member) =  guild.member(&ctx.c.http, user.id).await {
            let mut flags = Vec::new();
            if member.deaf {
                flags.push("Deaf");
            }
            if member.mute {
                flags.push("Mute");
            }
            if member.pending {
                flags.push("Pending");
            }
            body.push_str(&list_flags(flags));

            if let Some(nick) = member.nick {
                body.push_str(&format!("**Nick:** {}\n", nick));
            }

            if let Some(joined_at) = member.joined_at {
                body.push_str(&format!("**Joined:** {}\n", format_timestamp(*joined_at)));
            }

            if !member.roles.is_empty() {
                let mut roles = Vec::new();
                for role in member.roles {
                    roles.push(role.mention().to_string());
                }
                body.push_str(&format!("**Roles:** {}\n", roles.join(" ")));
            }

            if let Some(boosting_since) = member.premium_since {
                body.push_str(&format!("**Started boosting:** {}\n", format_timestamp(*boosting_since)));
            }

            if let Some(muted_until) = member.communication_disabled_until {
                body.push_str(&format!("**Muted until:** {}\n", format_timestamp(*muted_until)));
            }

            body.push('\n');

            if let Some(presence) = guild.presences.get(&user.id) {
                let status_string = match presence.status {
                    OnlineStatus::Online => "Online",
                    OnlineStatus::Idle => "Idle",
                    OnlineStatus::DoNotDisturb => "Do not disturb",
                    _ => "?",
                };
                body.push_str(&format!("**Status:** __{}__", status_string));

                if let Some(client_status) = &presence.client_status {
                    body.push_str(" on ");

                    let mut clients = Vec::new();
                    if let Some(desktop_status) = client_status.desktop {
                        clients.push(("desktop", desktop_status));
                    }
                    if let Some(web_status) = client_status.web {
                        clients.push(("web", web_status));
                    }
                    if let Some(mobile_status) = client_status.mobile {
                        clients.push(("mobile", mobile_status));
                    }

                    let mut clients_iter = clients.iter().peekable();
                    while let Some((platform, _)) = clients_iter.next() {
                        body.push_str(&format!("__{}__", platform));
                        if clients_iter.peek().is_some() {
                            body.push_str(", ");
                        }
                    }

                    body.push('\n');
                }

                if !presence.activities.is_empty() {
                    body.push_str("**Activities:** ");

                    let mut activities_iter = presence.activities.iter().peekable();
                    while let Some(activity) = activities_iter.next() {
                        body.push_str(&format!("{:?}", activity));
                    }

                    body.push('\n');
                }
            }
        }
    }

    let mut flags = Vec::new();
    if user.bot {
        flags.push("Bot");
    }
    body.push_str(&list_flags(flags));

    body.push_str(&format!(concat!(
        "**Created:** {}\n",
        "**ID:** `{}`",
    ), format_timestamp(*user.created_at()), user.id.as_u64()));

    let append_snowflake = !cmd.arguments["snowflake"].values.is_empty();
    if append_snowflake {
        body.push_str(&format_snowflake(user.id.0, true));
    }

    cmd.msg.channel_id.send_message(&ctx.c.http, |m| {
        m.add_embed(|e| {
            e.title(title)
                .description(body)
                .thumbnail(user.face())
                .image(user.banner_url().unwrap_or_default())
                .colour(user.accent_colour.unwrap_or_default())
        })
    }).await?;

    Ok(())
}

async fn guild(ctx: Context, cmd: Command) -> EventResult {
    let guild = cmd.msg.guild(&ctx.c.cache).unwrap();

    let mut body = String::new();

    body.push_str(&format!(concat!(
        "**Created:** {}\n",
        "**Owner:** {}\n",
        "**Member count:** {}\n",
        "**Channel count:** {}\n",
        "**Emoji count:** {}\n",
        "**Boost count:** {}\n",
        "**ID:** `{}`",
    ), format_timestamp(*guild.id.created_at()), guild.owner_id.mention(), guild.member_count, guild.channels.len(), guild.emojis.len(), guild.premium_subscription_count, guild.id.as_u64()));

    let append_snowflake = !cmd.arguments["snowflake"].values.is_empty();
    if append_snowflake {
        body.push_str(&format_snowflake(guild.id.0, true));
    }

    cmd.msg.channel_id.send_message(&ctx.c.http, |m| {
        m.add_embed(|e| {
            e.title(format!("👥 Guild info: `{}`", guild.name))
                .description(body)
                .thumbnail(guild.icon_url().unwrap_or_default())
                .image(guild.banner_url().unwrap_or_default())
        })
    }).await?;

    Ok(())
}

async fn message(ctx: Context, cmd: Command) -> EventResult {
    let message = if let Some(message_spec) = cmd.arguments["message"].try_value() {
        let (channel_id, message_id) = {
            let message_spec = message_spec.as_str().unwrap();
            let message_url_regex = Regex::new(r#"^https://discord.com/channels/(?:\d+|@me)/(\d+)/(\d+)$"#).unwrap();
            let (a, b) = if let Some(captures) = message_url_regex.captures(message_spec) {
                (captures[1].parse(), captures[2].parse())
            } else if let Some((a, b)) = message_spec.split_once(':') {
                (a.parse(), b.parse())
            } else {
                return Err("Cannot parse the message spec".into());
            };

            if let (Ok(a), Ok(b)) = (a, b) {
                (a, b)
            } else {
                return Err("Message spec parts are not integers".into());
            }
        };

        ctx.c.http.get_message(channel_id, message_id).await?
    } else if let Some(referenced_message) = cmd.msg.referenced_message {
        *referenced_message
    } else {
        return Err("No message spec or reply".into());
    };

    let mut body = String::new();

    let no_append_content = !cmd.arguments["no_content"].values.is_empty();
    if !no_append_content {
        for line in message.content.lines() {
            body.push_str(&format!("> {}\n", line));
        }
    }

    body.push_str(&format!("[Jump to message]({})\n", message.link()));

    let mut flags = Vec::new();
    if message.pinned {
        flags.push("Pinned");
    }
    if message.tts {
        flags.push("TTS");
    }
    if let Some(referenced_message) = &message.referenced_message {
        let user = &referenced_message.author;
        if message.mentions.iter().find(|u| u.id == user.id).is_some() {
            flags.push("Reply mention");
        }
    }
    body.push_str(&list_flags(flags));

    if let Some(referenced_message) = &message.referenced_message {
        body.push_str(&format!("**Referenced message ID:** `{}` ([Jump to message]({}))\n", referenced_message.id.0, referenced_message.link()));
    }

    body.push_str(&format!(concat!(
        "**User:** {}\n",
        "**Channel:** {}\n",
        "**Type:** {:?}\n",
        "**Reactions:** {:?}\n",
        "**Files:** {:?}\n",
        "**Stickers:** {:?}\n",
        "**Created:** {}\n",
    ), message.author.mention(), message.channel_id.mention(), message.kind, message.reactions, message.attachments, message.sticker_items, format_timestamp(*message.timestamp)));

    if let Some(edited_timestamp) = message.edited_timestamp {
        body.push_str(&format!("**Edited:** {}\n", format_timestamp(*edited_timestamp)));
    }

    body.push_str(&format!("**ID:** `{}`", message.id.0));

    let append_snowflake = !cmd.arguments["snowflake"].values.is_empty();
    if append_snowflake {
        body.push_str(&format!("\n{}", format_snowflake(message.id.0, true)));
    }

    cmd.msg.channel_id.send_message(&ctx.c.http, |m| {
        m.add_embed(|e| {
            e.title("🗨️ Message info")
                .description(body)
        })
    }).await?;

    Ok(())
}

async fn snowflake(ctx: Context, cmd: Command) -> EventResult {
    let snowflake = cmd.arguments["snowflake"].value().unwrap().as_u64().unwrap();

    cmd.msg.channel_id.send_message(&ctx.c.http, |m| {
        m.add_embed(|e| {
            e.title("❄️ Snowflake info")
                .description(format_snowflake(snowflake, false))
        })
    }).await?;

    Ok(())
}

pub fn register() -> Plugin {
    Plugin {
        events: Vec::new(),
        commands: HashMap::from([
            ("info".to_string(), cmdparse::Node::Command(cmdparse::Command {
                arguments: HashMap::new(),
                subcommands: HashMap::from([
                    ("user".to_string(), cmdparse::Node::Command(cmdparse::Command {
                        arguments: HashMap::from([
                            ("user".to_string(), cmdparse::Argument {
                                kind: cmdparse::ArgumentKind::Positional(0),
                                value: cmdparse::Value::U64,
                                default: None,
                                repeating: cmdparse::Repeat::False,
                                required: false,
                            }),
                            ("snowflake".to_string(), cmdparse::Argument {
                                kind: cmdparse::ArgumentKind::Option { long: Some(String::from("snowflake")), short: Some('s') },
                                value: cmdparse::Value::None,
                                default: None,
                                repeating: cmdparse::Repeat::False,
                                required: false,
                            }),
                        ]),
                        subcommands: HashMap::new(),
                        parse_negative_numbers: false,
                        metadata: Box::new(CommandMetadata {
                            callback: Some(Box::new(move |context, command| Box::pin(user(context, command)))),
                        }),
                    })),
                    ("guild".to_string(), cmdparse::Node::Command(cmdparse::Command {
                        arguments: HashMap::from([
                            ("snowflake".to_string(), cmdparse::Argument {
                                kind: cmdparse::ArgumentKind::Option { long: Some(String::from("snowflake")), short: Some('s') },
                                value: cmdparse::Value::None,
                                default: None,
                                repeating: cmdparse::Repeat::False,
                                required: false,
                            }),
                        ]),
                        subcommands: HashMap::new(),
                        parse_negative_numbers: false,
                        metadata: Box::new(CommandMetadata {
                            callback: Some(Box::new(move |context, command| Box::pin(guild(context, command)))),
                        }),
                    })),
                    ("server".to_string(), cmdparse::Node::Alias(vec!["info".to_string(), "guild".to_string()])),
                    ("message".to_string(), cmdparse::Node::Command(cmdparse::Command {
                        arguments: HashMap::from([
                            ("message".to_string(), cmdparse::Argument {
                                kind: cmdparse::ArgumentKind::Positional(0),
                                value: cmdparse::Value::String,
                                default: None,
                                repeating: cmdparse::Repeat::False,
                                required: false,
                            }),
                            ("snowflake".to_string(), cmdparse::Argument {
                                kind: cmdparse::ArgumentKind::Option { long: Some(String::from("snowflake")), short: Some('s') },
                                value: cmdparse::Value::None,
                                default: None,
                                repeating: cmdparse::Repeat::False,
                                required: false,
                            }),
                            ("no_content".to_string(), cmdparse::Argument {
                                kind: cmdparse::ArgumentKind::Option { long: Some(String::from("no-content")), short: Some('n') },
                                value: cmdparse::Value::None,
                                default: None,
                                repeating: cmdparse::Repeat::False,
                                required: false,
                            }),
                        ]),
                        subcommands: HashMap::new(),
                        parse_negative_numbers: false,
                        metadata: Box::new(CommandMetadata {
                            callback: Some(Box::new(move |context, command| Box::pin(message(context, command)))),
                        }),
                    })),
                    ("snowflake".to_string(), cmdparse::Node::Command(cmdparse::Command {
                        arguments: HashMap::from([
                            ("snowflake".to_string(), cmdparse::Argument {
                                kind: cmdparse::ArgumentKind::Positional(0),
                                value: cmdparse::Value::U64,
                                default: None,
                                repeating: cmdparse::Repeat::False,
                                required: true,
                            }),
                        ]),
                        subcommands: HashMap::new(),
                        parse_negative_numbers: false,
                        metadata: Box::new(CommandMetadata {
                            callback: Some(Box::new(move |context, command| Box::pin(snowflake(context, command)))),
                        }),
                    })),
                    ("id".to_string(), cmdparse::Node::Alias(vec!["info".to_string(), "snowflake".to_string()])),
                ]),
                parse_negative_numbers: false,
                metadata: Box::new(CommandMetadata {
                    callback: None,
                }),
            })),
        ]),
    }
}
